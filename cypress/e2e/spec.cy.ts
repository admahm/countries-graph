import "cypress-wait-until";

describe("Click all elements with", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000");
  });

  it("clicks on all elements", () => {
    // Initialize variables for tracking the number of elements
    let numContinentCircles = 0;
    let numEmojiGElements = 0;
    let numLanguageCircles = 0;

    // 1 Click on the first g element
    cy.get("g[data-testid*='world']")
      .first()
      .click()
      .should("exist", { timeout: 5000 }) // Wait up to 5 seconds for new elements to appear
      .then(() => {
        cy.get("circle[data-testid*='continent']").should("be.visible");
        cy.get("circle[data-testid*='continent']")
          .its("length")
          .should("be.above", numContinentCircles);
      });

    // 2 Click on all circles with data-testid containing "continent"
    cy.get("circle[data-testid*='continent']").each((element) => {
      cy.wrap(element)
        .click({ force: true })
        .then(() => {
          cy.waitUntil(
            () => {
              return cy.get("g[data-testid*='country']").then(($gElements) => {
                // Update numEmojiGElements if there are more emoji <g> elements than before
                if ($gElements.length > numEmojiGElements) {
                  numEmojiGElements = $gElements.length;
                  return true; // Return true to indicate that the condition is met
                } else {
                  return false; // Return false to indicate that the condition is not met
                }
              });
            },
            {
              timeout: 10000,
              interval: 500,
            }
          );
        });
    });

    // 3 Click on all g elements with text containing an emoji symbol
    cy.get("g[data-testid*='country']")
      .each((element) => {
        cy.wrap(element)
          .click({ force: true })
          .then(() => {
            cy.waitUntil(
              () =>
                 {
                  return cy.get("circle[data-testid*='language']").then(($gElements) => {
                    if ($gElements.length > numLanguageCircles) {
                      numLanguageCircles = $gElements.length;
                      return true; // Return true to indicate that the condition is met
                    
                    }
                    else {
                      return true; // Return true because we want to keep the test going and clicks on countries don't always yield new languages
                    }
                  });
                },
              {
                timeout: 10000,
                interval: 500,
              }
            );
          });
      });
  });
});
