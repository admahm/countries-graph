import { gql } from "@apollo/client";

export const GET_CONTINENTS = gql`
  query GetContinents {
    continents {
      code
      name
    }
  }
`;

export const GET_COUNTRIES_FOR_CONTINENT = gql`
  query GetCountriesForContinent($code: String!) {
    countries(filter: { continent: { eq: $code } }) {
      code
      name
      emoji
      languages {
        code
        name
      }
    }
  }
`;

export const GET_LANGUAGES_FOR_COUNTRY = gql`
  query GetLanguagesForCountry($code: ID!) {
    languages: country(code: $code) {
      languages {
        code
        name
      }
    }
  }
`;
