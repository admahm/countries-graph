import { InputNode } from "@nivo/network";

export interface DataNode extends InputNode {
    name: string;
    code:string
    type: string;
    size: number;
    fill: string;
    emoji?: string;
    emojiImgUrl?:string
  }
  
  export interface Link {
    id:string;
    source: string;
    target: string;
  }
  
  export interface Data {
    nodes: DataNode[];
    links: Link[];
  }

  export interface ChartProps {
    data: Data;
    onNodeClick?: (node: DataNode) => void;
  }