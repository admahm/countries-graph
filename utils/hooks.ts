import { useCallback, useEffect, useState } from "react";
import { ApolloError, NetworkStatus, useApolloClient } from "@apollo/client";
import {
  GET_CONTINENTS,
  GET_COUNTRIES_FOR_CONTINENT,
  GET_LANGUAGES_FOR_COUNTRY,
} from "./queries";
import { GraphQLError } from "graphql";
import { DataNode, Data, Link } from "./DataInterfaces";

export const useCountriesAPI = (
  firstNode: DataNode
): [
  Data | undefined,
  (parentNode: DataNode, linkToParent?: boolean) => void
] => {
  const client = useApolloClient();
  const [data, setData] = useState<Data>({ nodes: [firstNode], links: [] });

  //generate emoji img for first node
  useEffect(() => {
    console.log("Useeffect inside Hook");
    if (firstNode.emoji) {
      const firstNodewithEmoji = {
        ...firstNode,
        emojiImgUrl: getEmojiImgUrl(firstNode.emoji),
      };
      setData((data) => ({
        nodes: addNodes(data?.nodes ?? [], [firstNodewithEmoji]),
        links: data.links,
      }));
    }
  }, []);

  const fetchData = useCallback(
    function (parentNode: DataNode, linkToParent: boolean = true) {
      client
        .query(getQuery(parentNode))
        .then((result) => {
          const nodes: DataNode[] = createNodes(result);
          let links: Link[] = [];
          setData((data) =>
            changeNodeSizes({
              nodes: addNodes(addNodes(data?.nodes ?? [], nodes), [parentNode]),
              links: linkToParent
                ? generateAndAddLinks(data?.links ?? [], nodes, parentNode)
                : links,
            })
          );
        })
        .catch((error) => {
          console.error(error);
        });
    },
    [client, data]
  );

  return [data, fetchData];
};

function createNodes(result: {
  data: any;
  errors?: readonly GraphQLError[] | undefined;
  error?: ApolloError | undefined;
  loading?: boolean;
  networkStatus?: NetworkStatus;
  partial?: boolean | undefined;
}) {
  const {
    continents = [],
    countries = [],
    languages: { languages = [] } = {},
  } = result.data ?? {};

  // Convert the cache objects into nodes
  const nodes: DataNode[] = [
    ...Object.values(continents).map((continent: any) => ({
      id: continent.code + "continent",
      code: continent.code,
      name: continent.name,
      type: "continent",
      size: 10,
      fill: "#00509d",
    })),
    ...Object.values(countries).map((country: any) => ({
      id: country.code + "country",
      code: country.code,
      name: country.name,
      type: "country",
      size: 15,
      fill: "#3d83c6",
      emoji: country.emoji,
      emojiImgUrl: getEmojiImgUrl(country.emoji),
    })),
    ...Object.values(languages).map((language: any) => ({
      id: language.code + "language",
      code: language.code,
      name: language.name,
      type: "language",
      size: 5,
      fill: "#94b5e3",
    })),
  ];
  return nodes;
}

function getQuery({ type, code }: DataNode) {
  switch (type) {
    case "world":
      return { query: GET_CONTINENTS };
    case "continent":
      return {
        query: GET_COUNTRIES_FOR_CONTINENT,
        variables: { code },
      };
    case "country":
      return {
        query: GET_LANGUAGES_FOR_COUNTRY,
        variables: { code },
      };
    default:
      throw new Error(`Invalid type: ${type}`);
  }
}

function addNodes(originalNodes: DataNode[], newNodes: DataNode[]): DataNode[] {
  const mergedNodes: DataNode[] = [];

  for (const originalNode of originalNodes) {
    const newNode = newNodes.find((node) => node.id === originalNode.id);
    mergedNodes.push(newNode ?? originalNode);
  }

  for (const newNode of newNodes) {
    const nodeExists = mergedNodes.some((node) => node.id === newNode.id);
    if (!nodeExists) {
      mergedNodes.push(newNode);
    }
  }

  return mergedNodes;
}


function generateAndAddLinks(
  links: Link[],
  nodes: DataNode[],
  parent: DataNode
): Link[] {
  const newLinks: Link[] = nodes.map((node) => ({
    id: `${parent.id}-${node.id}`,
    source: parent.id,
    target: node.id,
  }));

  const allLinks = [...links, ...newLinks];
  const uniqueLinks = Array.from(new Set(allLinks.map((link) => link.id))).map(
    (id) => allLinks.find((link) => link.id === id)!
  );

  return uniqueLinks;
}

function changeNodeSizes(data: Data): Data {
  console.time("changeNodeSizes"); // Start the timer
  const newNodes = data.nodes.map((node) => {
    if (node.type !== "language") {
      return node;
    }
    const numLinks = data.links.filter(
      (link) => link.source === node.id || link.target === node.id
    ).length;
    const newSize = numLinks > 0 ? 5 * numLinks : 5;
    return { ...node, size: newSize };
  });
  console.timeEnd("changeNodeSizes"); // End the timer and log the duration
  return { ...data, nodes: newNodes };
}

export function getEmojiImgUrl(emoji: string): string {
  const canvas = document.createElement("canvas");
  canvas.width = 92;
  canvas.height = 92;
  const context = canvas.getContext("2d");

  // Check if the context is not null
  if (context !== null) {
    // Draw the emoji in the center of the canvas
    context.font = "64px serif";
    context.textAlign = "center";
    context.textBaseline = "middle";
    context.fillText(emoji, canvas.width / 2, canvas.height / 2);

    // Convert the canvas to a base64 encoded image
    const dataUrl = canvas.toDataURL("image/png");
    const base64Image = dataUrl.split(",")[1];

    return `data:image/png;base64,${base64Image}`;
  } else {
    throw new Error("Could not get canvas context");
  }
}

export const useMockData = (firstNode: DataNode): Data | undefined => {
  const client = useApolloClient();
  const [data, setData] = useState<Data>({ nodes: [firstNode], links: [] });

  // generate emoji img for first node
  useEffect(() => {
    console.log("Useeffect inside Hook");
    if (firstNode.emoji) {
      const firstNodewithEmoji = {
        ...firstNode,
        emojiImgUrl: getEmojiImgUrl(firstNode.emoji),
      };
      setData((data) => ({
        nodes: addNodes(data?.nodes ?? [], [firstNodewithEmoji]),
        links: data.links,
      }));
    }
  }, []);

  function fetchData(parentNode: DataNode, linkToParent: boolean = true) {
    client
      .query(getQuery(parentNode))
      .then((result) => {
        const nodes: DataNode[] = createNodes(result);
        let links: Link[] = [];
        setData((data) =>
          changeNodeSizes({
            nodes: addNodes(addNodes(data?.nodes ?? [], nodes), [parentNode]),
            links: linkToParent
              ? generateAndAddLinks(data?.links ?? [], nodes, parentNode)
              : links,
          })
        );
        nodes
          .filter((node) => node.type !== "language")
          .forEach((node) => fetchData(node));
      })
      .catch((error) => {
        console.error(error);
      });
  }

  useEffect(() => {
    fetchData(firstNode);
  }, [firstNode]);

  return data;
};
