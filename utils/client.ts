import { ApolloClient, ApolloProvider, InMemoryCache, HttpLink } from '@apollo/client';

const httpLink = new HttpLink({ uri: 'https://countries.trevorblades.com/graphql' });
export const client = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache(),
});