export default {
  component: {
    devServer: {
      framework: "next",
      bundler: "webpack",
    },
  },

  e2e: {
    setupNodeEvents(on: any, config: any) {
      // implement node event listeners here
    },
  },
};
