import React from "react";
import { useCountriesAPI } from "@/utils/hooks";
import { NetworkChart } from "./nivoChart/NetworkChart";
import { NetworkRegraph } from "./Rechart/NetworkRegraph";
import { DataNode } from "@/utils/DataInterfaces";
const world: DataNode = {
  id: "worldworld",
  code: "world",
  name: "world",
  type: "world",
  size: 10,
  fill: "#c83a43",
  emoji: "🌍",
};
export const DataLoader = ({ webGL }: { webGL: boolean }) => {
  const [data, refetch] = useCountriesAPI(world);
  return data ? (
    webGL ? (
      <NetworkRegraph
        onNodeClick={(node) => node.type !== "language" && refetch(node)}
        data={data}
      />
    ) : (
      <NetworkChart
        onNodeClick={(node) => node.type !== "language" && refetch(node)}
        data={data}
      />
    )
  ) : (
    <p>No Data!</p>
  );
};
