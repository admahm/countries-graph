import { ChartProps } from "@/utils/DataInterfaces";
import dynamic from "next/dynamic";

const GraphCanvas = dynamic(
  () => import("reagraph").then((module) => module.GraphCanvas),
  {
    ssr: false,loading: () => <div>Loading...</div>
  }
);

const SphereWithIcon = dynamic(
  () => import("reagraph").then((module) => module.SphereWithIcon),
  {
    ssr: false,
  }
);

const Sphere = dynamic(
  () => import("reagraph").then((module) => module.Sphere),
  {
    ssr: false,
  }
);

export const NetworkRegraph = ({
  data,
  onNodeClick,
}: ChartProps): JSX.Element => (
  <GraphCanvas
    nodes={data.nodes}
    edges={data.links}
    edgeInterpolation="curved"
    edgeArrowPosition="none"
    sizingType="pagerank"
    layoutType="forceDirected2d"
    draggable
    onNodeClick={({ data }) => {
      if (onNodeClick) {
        onNodeClick(data);
      }
    }}
    renderNode={({ size, color, opacity, node }) => (
      <group>
        {node.data.emojiImgUrl ? (
          <>
            <SphereWithIcon
              image={node.data.emojiImgUrl}
              color={color}
              node={node}
              size={size/2 || 5}
              active={true}
              animated={true}
              id={node.id}
              opacity={0}
            />
          </>
        ) : (
          <>
            <Sphere
              color={color}
              node={node}
              size={size/2 || 5}
              active={true}
              opacity={opacity}
              animated={true}
              id={node.id}
            />
          </>
        )}
      </group>
    )}
  />
);
