import { useCountriesAPI, useMockData } from "../../utils/hooks";
import React, { useEffect } from "react";
import { NetworkRegraph } from "./NetworkRegraph";
import { DataNode } from "@/utils/DataInterfaces";
import { ApolloProvider } from "@apollo/client";
import { client } from "../../utils/client";

export const world: DataNode = {
  id: "worldworld",
  code: "world",
  name: "world",
  type: "world",
  size: 10,
  fill: "#c83a43",
  emoji: "🌍",
};

const MockComponent = (node:DataNode) => {
  const data = useMockData(node);

return data ? (
  <div>
  <NetworkRegraph
    onNodeClick={(node) => {console.log('clicked', node)}}
    data={data}
  /></div>
) : (
  <p>No Data!</p>
);

};

describe("<NetworkRegraph />", () => {

  it.only("renders and finds Component", () => {
    cy.mount(
      <ApolloProvider client={client}>
        <MockComponent {...world} />
      </ApolloProvider>
    );

  });
});

