import { DataNode } from "@/utils/DataInterfaces";
import { InputNode, NodeTooltipProps } from "@nivo/network";
import React, { FunctionComponent } from "react";

const NodeTooltip: FunctionComponent<NodeTooltipProps<InputNode>> = ({
  node,
}) => {
  const {emoji,name}=node.data as DataNode
  return (
    <div
      id={node.id}
      style={{
        background: "white",
        border: "1px solid black",
        padding: "8px",
        borderRadius: "4px",
        borderColor: node.borderColor,
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        gap:"10px"
      }}
    >
      {emoji && <div>{emoji}</div>}
      <div>{name}</div>
    </div>
  );
};

export default NodeTooltip;
