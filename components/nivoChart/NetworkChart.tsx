import { DataNode, ChartProps } from "@/utils/DataInterfaces";
import dynamic from "next/dynamic";
import NodeTooltip from "./costomNodeTooltip";
import { CustomNode } from "./customNode";


const ResponsiveNetwork = dynamic(
  () => import('@nivo/network').then((module) => module.ResponsiveNetwork),
  { ssr: false, loading: () => <div>Loading...</div>}
);

export const NetworkChart = ({
  data,
  onNodeClick,
}: ChartProps): JSX.Element => {
  return (
    <>
      <ResponsiveNetwork
        data={data}
        margin={{ top: 0, right: 0, bottom: 0, left: 0 }}
        centeringStrength={0.4}
        repulsivity={20}
        nodeSize={(n) => (n as DataNode).size}
        activeNodeSize={(n) => 1.5 * (n as DataNode).size}
        inactiveNodeSize={(n) => 0.9 * (n as DataNode).size}
        nodeColor={(n) => (n as DataNode).fill}
        nodeBorderWidth={1}
        nodeBorderColor={{ from: "fill", modifiers: [["darker", 0.8]] }}
        linkThickness={2}
        linkBlendMode="multiply"
        onClick={({ data }) => onNodeClick && onNodeClick(data as DataNode)}
        nodeTooltip={NodeTooltip}
        distanceMax={60}
        nodeComponent={CustomNode}
        iterations={100}
        distanceMin={1}
        animate={true}
      />
    </>
  );
};
