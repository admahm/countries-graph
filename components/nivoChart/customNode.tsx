import React from "react";
import { ComputedNode, InputNode, NodeProps } from "@nivo/network";

import { memo } from "react";
import { animated, to } from "@react-spring/web";
import { DataNode } from "@/utils/DataInterfaces";

const NonMemoizedNetworkNode = <Node extends InputNode>({
  node,
  animated: animatedProps,
  onClick,
  onMouseEnter,
  onMouseMove,
  onMouseLeave,
}: NodeProps<Node>) => {
  const data=node.data as unknown as DataNode;
  return (
    data.emoji ?
    <animated.g
      data-testid={node.id}
      transform={to(
        [animatedProps.x, animatedProps.y, animatedProps.scale],
        (x, y, scale) => {
          return `translate(${x},${y}) scale(${scale})`;
        }
      )}
      onClick={onClick ? (event) => onClick(node, event) : undefined}
      onMouseEnter={
        onMouseEnter ? (event) => onMouseEnter(node, event) : undefined
      }
      onMouseMove={
        onMouseMove ? (event) => onMouseMove(node, event) : undefined
      }
      onMouseLeave={
        onMouseLeave ? (event) => onMouseLeave(node, event) : undefined
      }
      style={{ cursor: onClick ? 'pointer' : 'default' }}
    >
      <text dominantBaseline="central" textAnchor="middle">
        {data.emoji}
      </text>
    </animated.g>
    :
    <animated.circle
      data-testid={`node.${node.id}`}
      transform={to(
        [animatedProps.x, animatedProps.y, animatedProps.scale],
        (x, y, scale) => {
          return `translate(${x},${y}) scale(${scale})`;
        }
      )}
      r={to([animatedProps.size], (size) => size / 2)}
      fill={animatedProps.color}
      strokeWidth={animatedProps.borderWidth}
      stroke={animatedProps.borderColor}
      opacity={animatedProps.opacity}
      onClick={onClick ? (event) => onClick(node, event) : undefined}
      onMouseEnter={
        onMouseEnter ? (event) => onMouseEnter(node, event) : undefined
      }
      onMouseMove={
        onMouseMove ? (event) => onMouseMove(node, event) : undefined
      }
      onMouseLeave={
        onMouseLeave ? (event) => onMouseLeave(node, event) : undefined
      }
      style={{ cursor: onClick ? 'pointer' : 'default' }}
    />
  );
  
};

export const CustomNode = memo(
  NonMemoizedNetworkNode
) as typeof NonMemoizedNetworkNode;