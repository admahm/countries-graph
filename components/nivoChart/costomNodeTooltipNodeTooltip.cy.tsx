import { mount } from 'cypress/react18';
import React from 'react';
import NodeTooltip from './costomNodeTooltip';


describe('NodeTooltip component', () => {
  const dataNode = {
    id: '1',
    name: 'Node 1',
    code: 'A',
    type: 'type1',
    size: 50,
    fill: '#ff0000',
    emoji: '😊',
  };

  const computedNode = {
    id: dataNode.id,
    index: 0,
    x: 0,
    y: 0,
    vx: 0,
    vy: 0,
    size: dataNode.size,
    radius: 0,
    depth: 0,
    color: dataNode.fill,
    label: "",
    data: dataNode,
    borderWidth: 1,
    borderColor: "black",
  };

  it('displays the node name', () => {
    mount(<NodeTooltip node={computedNode} />);
    cy.contains('div', dataNode.name).should('be.visible');
  });

  it('displays the emoji if provided', () => {
    mount(<NodeTooltip node={computedNode} />);
    cy.contains('div', dataNode.emoji).should('be.visible');
  });

  it('does not display the emoji if not provided', () => {
    const dataNodeWithoutEmoji = {
      ...dataNode,
      emoji: undefined,
    };
    mount(<NodeTooltip node={computedNode} />);
    cy.contains('div', dataNodeWithoutEmoji.emoji).should('not.exist');
  });

  it('has a white background color', () => {
    mount(<NodeTooltip node={computedNode} />);
    cy.get('#'+computedNode.id).should('have.css', 'background-color', 'rgb(255, 255, 255)');
  });

  it('has a black border', () => {
    mount(<NodeTooltip node={computedNode} />);
    cy.get('#'+computedNode.id).should('have.css', 'border-color', 'rgb(0, 0, 0)');
  });

  it('has a padding of 8px', () => {
    mount(<NodeTooltip node={computedNode} />);
    cy.get('#'+computedNode.id).should('have.css', 'padding', '8px');
  });

  it('has a border radius of 4px', () => {
    mount(<NodeTooltip node={computedNode} />);
    cy.get('#'+computedNode.id).should('have.css', 'border-radius', '4px');
  });
});
