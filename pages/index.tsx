import Head from "next/head";
import { Inter } from "next/font/google";
import styles from "@/styles/Home.module.css";
import { ApolloProvider } from "@apollo/client";
import { client } from "@/utils/client";
import Switch from "react-switch";
import { useState } from "react";
import { DataLoader } from "@/components/dataLoader";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {

  const [webGL, setWebGL] = useState<boolean>(false);


  return (
    <>
      <Head>
        <title>Countries Api Graph</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/CountriesApiGraph.ico" />
      </Head>
      <main className={styles.main}>
        <div className={styles.description}>
          <div className={styles.box}>   
            <label className={styles.center}>
              <span>SVG / WebGL</span>
              <Switch
                onChange={setWebGL}
                checked={webGL}
              />
            </label>
          </div>
          <div>
            By &nbsp;<code className={styles.code}>Adam Mahmoud</code>
          </div>
        </div>

        <div className={`${styles.center} ${styles.chart}`}>
          <ApolloProvider client={client}>
            <DataLoader webGL={webGL}/>
          </ApolloProvider>
        </div>

        <div>
          <div>
            <h1 className={inter.className}>Countries API Graph</h1>
            <p className={inter.className}>
              The Graph above works by using CountriesAPI to fetch Continents,
              Countries and Languages. To try click on the Nodes and see how
              it&apos;s connected!
            </p>
          </div>
        </div>
      </main>
    </>
  );
}
