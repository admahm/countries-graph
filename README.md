## Countries-Graph

This is a small web application created mainly for practice. It uses the GraphQL API at [Countries API](countries.trevorblades.com) to get information about Countries, and displays them in a nice looking Graph.
There is a SVG and WebGL (Experimental) Graph to choose from.

The app is created using [Next.js](https://nextjs.org/) bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).
It also uses reagraph,  Nivo and Cypress for testing.

## Getting Started



First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.